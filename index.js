const scheduler = require('node-schedule');
const fs = require('fs');
const _ = require('lodash');
const moment = require('moment');

const totalDays = moment().daysInMonth();
const currentMonth = moment().month();
const currentYear = moment().year();

let totalTales = 0;
let totalWriters = 0;
let writers = [];
let writerData = {};
let talesToPublish = {};
let writersWhoWouldLeave = [];

function readFile() {
    return new Promise((resolve, reject) => {
        fs.readFile('Writer-submission-distribution.csv', (error, data) => {
            const fileData = data.toString().split('\n');
            totalTales = fileData.length;

            fileData.forEach(row => {
                let [writer_id, tale_id] = row.split(',');
                if (writerData[writer_id]) {
                    writerData[writer_id].tales.push(tale_id.replace('\r', ''));
                    writerData[writer_id].totalTales = writerData[writer_id].totalTales + 1;
                    writerData[writer_id].lastTalePublished = writerData[writer_id].lastTalePublished + 1;
                    writerData[writer_id].happinessIndex = writerData[writer_id].happinessIndex + 1;
                } else
                    writerData[writer_id] = { tales: [tale_id.replace('\r', '')], writer_id, earnings: 0, lastTalePublished: 7, happinessIndex: 1, totalTales: 1, talesPublished: 0 };
            });

            writerData = _.sortBy(writerData, (writer) => writer.tales.length);
            writers = Object.keys(writerData);
            totalWriters = writers.length;
            resolve();
        });
    });
}

function checkForOverallHappiness() {
    return new Promise((resolve, reject) => {
        /** If selected writer has lTP > 0 and there is a writer with lTP = 0, consider writer with lTP = 0 */
        const writerAboutToLeave = _.find(writerData, (writer) => writer.lastTalePublished == 0);
        if (writerAboutToLeave && writerAboutToLeave.tales.length)
            resolve({ result: false, writer: writerAboutToLeave });
        else
            /** Remaining writers are happy */
            resolve({ result: true });
    });
}

async function considerWriter(WRITER, writersWhoseTalePublished, day) {
    talesToPublish[day] ? talesToPublish[day].push(WRITER.tales[0]) : talesToPublish[day] = [WRITER.tales[0]];
    writersWhoseTalePublished.push(WRITER.writer_id);
    // WRITER.talesPublished++;
    WRITER.tales.pop();
    return;
}

async function selectUnhappyWriter(writerArray, day) {
    /** Maximum for 7 days */
    let i = 7;
    while (i > 0) {
        /** Select writer who has not published that day, has remaining tales and whose lastTale was published i days ago */
        writer = await _.find(writerData, (writerInfo) => {
            return writerArray.indexOf(writerInfo.writer_id) == -1 && writerInfo.lastTalePublished <= i && writerInfo.tales.length;
        });
        if (writer)
            return writer;

        /** Select writer who has not published that day, has remaining tales and whose lastTale was published i days ago and earnings = 0 */
        writer = await _.find(writerData, (writerInfo) => {
            return writerArray.indexOf(writerInfo.writer_id) == -1 && writerInfo.lastTalePublished <= i && writerInfo.tales.length && writerInfo.earnings == 0;
        });
        if (writer)
            return writer;

        i--;
    }
    return -1;
}

async function selection(index, WRITER, day, writersWhoseTalePublished, writer_id) {

    if (WRITER.lastTalePublished == 0) {
        /** Consider his tale */
        await considerWriter(WRITER, writersWhoseTalePublished, day);
        return true;
    } else {
        const happinessResult = await checkForOverallHappiness();

        if (!happinessResult.result) {
            if (index == totalTales - 1) {
                console.log('Deadlock');
                /** Consider him because no option */
                return false;
            } else {
                /** Revert, don't consider this writer as this will make overall happiness low */
                WRITER.earnings -= 1000;
                WRITER.happinessIndex -= 1;
                WRITER.lastTalePublished -= 1;

                /** Instead select other writer with lTP = 0 */
                await considerWriter(happinessResult.writer, writersWhoseTalePublished, day);
                return true;
            }
        } else {
            /** Consider his tale */
            await considerWriter(WRITER, writersWhoseTalePublished, day);
            return true;
        }
    }
}

async function selectTales(day, shuffleCount) {

    let writersWhoseTalePublished = [];
    let index = 0;
    while (index < totalTales) {
        let writer_id = writers[index % totalWriters];
        let WRITER = writerData[writer_id];

        /** Consider tales linearily */
        if (WRITER && WRITER.tales.length > 0 && WRITER.totalTales - WRITER.talesPublished != 0) {

            WRITER.earnings += 1000;
            WRITER.happinessIndex += 1;
            WRITER.lastTalePublished += 1;
            const selectionResult = await selection(index, WRITER, day, writersWhoseTalePublished, writer_id);

            if (selectionResult) {
                if (talesToPublish[day].length == 10) {
                    /** Increase lastTalePublished day for other writers */
                    for (const writer_id in writerData) {
                        if (writersWhoseTalePublished.indexOf(writer_id) == -1) {
                            writerData[writer_id].happinessIndex -= 1;
                            writerData[writer_id].lastTalePublished -= 1;
                        }
                    }
                    index = index + totalTales + 1;     // Break
                }
                else
                    index++;                            // Next iteration
            } else
                index++;
        }
        /** If last index and still not 10 tales, shuffle array and again choose linerily */
        else if (index == totalTales - 1) {
            /** Shuffled 5 times, still not 10 tales */
            if (shuffleCount == 0) {
                /** Select remaining tales considering happinessIndex */
                let remainingTales = 10 - talesToPublish[day].length;

                while (remainingTales > 0) {
                    const unhappyWriter = await selectUnhappyWriter(writersWhoseTalePublished, day);
                    if (unhappyWriter) {
                        // console.log('Found unhappy writer', unhappyWriter);
                        considerWriter(unhappyWriter, writersWhoseTalePublished, day);
                    } else {
                        /** unhappyWriter = -1 */
                        // console.log('No unhappy writer found');
                        /** Re-start loop */
                        index = 0;
                    }
                }
                index = index + totalTales + 1;

            } else {
                /** Shuffle and choose */
                _.shuffle(writerData);
                shuffleCount--;
                await selectTales(day, shuffleCount);
            }

        } else
            index++;                                    // Next iteration

    }
    return writersWhoseTalePublished;
}

readFile()
    .then(() => {
        (async () => {
            console.log('Total writers:-', totalWriters);
            console.log('Total tales:-', totalTales);
            console.log(`Total days:- ${totalDays}`);

            let day = 1;
            while (day < totalDays) {

                talesToPublish[day] = [];
                let writersWhoseTalePublished = await selectTales(day, 5);
                let unhappyWriter = 0;

                for (const writer of writerData) {
                    /** Unhappy writers for current day are writers who haven't published since last 7 days */
                    if (writersWhoseTalePublished.indexOf(writer.writer_id) == -1 && writer.tales.length && writer.lastTalePublished == 0 && writer.earnings == 0) {
                        unhappyWriter++;
                        writersWhoWouldLeave = [...writersWhoWouldLeave, writer.writer_id];
                    }
                    /** Pop out writers whose tales published today */
                    if (writersWhoseTalePublished.indexOf(writer.writer_id) > -1)
                        writersWhoWouldLeave.pop(writer.writer_id);
                }

                // writersWhoWouldLeave.forEach(writer_id => {
                //     let writer = _.find(writerData, (writer) => writer.writer_id == writer_id);
                //     if (writer.talesPublished < parseInt(writer.totalTales / 2))
                //         writersWhoWouldLeave.pop(writer_id);
                // });

                console.log(`DAY ${day} | Writers leaving TTT :- ${unhappyWriter} | Tales published:- ${talesToPublish[day]}`);
                day++;
            }
            console.log('\nUnhappy writers in the end :-', writersWhoWouldLeave);
            createSchedule(day);
        })();
    });


function createSchedule(day) {
    scheduler.scheduleJob(`Publish_Tales_Day_${day}`, `0 0 0 ${day} ${currentMonth} ${currentYear}`, (fireDate) => {
        console.log(`Publishing tales ${talesToPublish[day]} on ${fireDate}`);
    });
}