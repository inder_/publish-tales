# Publish Tales

A script that selects 10 tales from a pool of selected tales to publish every day

## Installation


Use the node package manager [npm](https://www.npmjs.com/) to install dependencies.

```bash
npm install 
```

Good to go! Now run the following command to run the app

```bash
node index.js
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
